import React, {useState, useEffect} from 'react'
//on importe nos fonction pour modifier ou lire nos state globale présentes dans le store de redux
import {useDispatch, useSelector} from 'react-redux'
//selectName récupère la state, et changeName l'action de modification
import {selectName, changeName} from '../slices/nameSlice'
import {selectNumber, addOne, removeOne, reset} from '../slices/numberSlice'
import {selectProduct, getAllProducts} from '../slices/productSlice'

import axios from 'axios'

const Page1 = (props) => {
    
    //récupération d'une state pour la lecture (recup moi la state name)
    const myName = useSelector(selectName)
    const myCount = useSelector(selectNumber)
    const myProduct = useSelector(selectProduct)
    
    //on initialise notre fonction dispatcher pour modifier une state via une action
    const dispatch = useDispatch()
    
    const [name, setName] = useState("")
    
    const handleSubmit = (e) => {
        e.preventDefault()
        //on dispatch l'événement qui va déclencher l'action de modification de nom en envoyant la nouvelle valeur dans l'argument
        dispatch(changeName(name))
    }
    
    useEffect(()=>{
        axios.get('https://jsonplaceholder.typicode.com/photos?albumId=1')
            .then((response)=>{
                //dispatch déclenche l'événement, getAllproducts est l'action du slice et response.data est le fameux payload
                dispatch(getAllProducts(response.data)) //je dispatch directement getAllProduct 
            })
            .catch((err)=>{
                console.log(err)
            })
    },[])
    console.log(myProduct)
    return (<main>
        <h1>Page 1</h1>
        <form
            onSubmit={handleSubmit}
        >
            <input
                type="text"
                placeholder={myName.name}
                onChange={(e)=>{
                    setName(e.currentTarget.value)
                }}
            />
        </form>
        
        <p>{myCount.count}</p>
        <button
            onClick={(e)=>{
                if(myCount.count > 0){
                    let newCount = myCount.count - 1 
                    dispatch(removeOne(newCount))
                }
            }}
        >
            décrémenter
        </button>
        <button
            onClick={(e)=>{
                let newCount = myCount.count + 1 
                dispatch(addOne(newCount))
            }}
        >
            incrémenter
        </button>
        <button
            onClick={(e)=>{
                dispatch(reset())
            }}
        >
            reset
        </button>
        
        {myProduct.product !== null && <div className="list-item">
            {myProduct.product.map((product)=>{
                return (<li key={product.id}>
                    <img src={product.thumbnailUrl} />
                    <h2>{product.title}</h2>
                </li>)
            })}
        </div>}
    </main>)
}

export default Page1