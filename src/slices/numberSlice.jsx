import {createSlice} from '@reduxjs/toolkit'

//on crée une state initiale
const initialState = {
    count: 0
}

export const numberSlice = createSlice({
    name: "number", //on nomme la state
    initialState, //on initialise la valeur de la state
    reducers: {
        addOne: (state, action) =>{
            state.count = action.payload
        },
        removeOne : (state, action) =>{
            state.count = action.payload
        },
        reset: (state) =>{
            state.count = 0
        }
    } //on crée nos actions qui vont mettre à jour nos reducers
})

//je déclare officiellement que ma fonction changeName de reducers sera une action (pour modifier mes states)
export const {addOne, removeOne, reset} = numberSlice.actions //on pourra modifier les states en appelant les actions depuis le composant

//on indique le nom de la state qu'on pourra manipuler dans le composant
export const selectNumber = state => state.number

//on exporte le slice en tant que reducer afin de l'injecter dans le store et rendre la state manipulable.
export default numberSlice.reducer