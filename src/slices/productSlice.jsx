import {createSlice} from '@reduxjs/toolkit'

//on crée une state initiale
const initialState = {
    product: null
}

export const productSlice = createSlice({
    name: "product", //on nomme la state
    initialState, //on initialise la valeur de la state
    reducers: {
        loadAllProducts: (state, action) => {
            state.product = action.payload
        }
    }
})

//je déclare officiellement que ma fonction changeName de reducers sera une action (pour modifier mes states)
export const {loadAllProducts} = productSlice.actions //on pourra modifier les states en appelant les actions depuis le composant

//on indique le nom de la state qu'on pourra manipuler dans le composant
export const selectProduct = state => state.product

//on exporte le slice en tant que reducer afin de l'injecter dans le store et rendre la state manipulable.
export default productSlice.reducer