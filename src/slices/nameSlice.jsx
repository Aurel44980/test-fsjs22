import {createSlice} from '@reduxjs/toolkit'

//on crée une state initiale
const initialState = {
    name: "Gérard"
}

//je crée ma state via creatSlice qui va initiliser un reducer avec une state et qui aura les fonctions de modifications incluses (actions)
export const nameSlice = createSlice({
    name: "name", //on nomme notre state
    initialState, //on initialise la state avec la valeur par défaut de notre const;
    reducers: {
        changeName: (state, action) => {
            state.name = action.payload
        } //on crée nos actions qui vont mettre à jour nos reducers
    }
})

//je déclare officiellement que ma fonction changeName de reducers sera une action (pour modifier mes states)
export const {changeName} = nameSlice.actions //on pourra modifier les states en appelant les actions depuis le composant

//on indique le nom de la state qu'on pourra manipuler dans le composant
export const selectName = state => state.name

//on exporte notre slice en taént que reducer pour l'injecter 
export default nameSlice.reducer