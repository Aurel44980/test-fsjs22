import {configureStore} from '@reduxjs/toolkit'
//on importe nos reducers pour les injecter dans le store
import nameReducer from './nameSlice'
import numberReducer from './numberSlice'
import productReducer from './productSlice'

//redux a simplifié le branchement du store avec une seule fonction qui connecte le store en récupérant les reducers et en branchant la console de redux devtools
const store = configureStore({
    reducer: {
        name: nameReducer,
        number: numberReducer,
        product: productReducer
    }
})

export default store