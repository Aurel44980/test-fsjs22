import logo from './logo.svg';
import './App.css';
import Header from './components/header'
import Page1 from './containers/page1'
import Page2 from './containers/page2'
import Page3 from './containers/page3'
import {Routes, Route} from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Header/>
      <Routes>
        <Route exact path="/" element={<Page1 />}/>
        <Route exact path="/page2" element={<Page2 />}/>
        <Route exact path="/page3" element={<Page3 />}/>
      </Routes>
    </div>
  );
}

export default App;
